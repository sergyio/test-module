;
// function getParent(el, parentTagName) {
//     var obj = el;
//     while (obj.tagName !== parentTagName) {
//         obj = obj.parentNode;
//     }
//     return obj;
// }

var archBtn;
archBtnArr = document.querySelectorAll('.set-archive');

archBtnArr.forEach(function (archBtn) {
    archBtn.addEventListener('click', function (e) {
        e.preventDefault();
        var el = e.target,
            path = el.getAttribute('href'),
            disabled = el.getAttribute('disabled');
        if (disabled === 'disabled') {
            return false;
        } else {
            var request = new XMLHttpRequest();
            request.open('POST', path, true);
            request.setRequestHeader("X-Requested-With","XMLHttpRequest");
            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    // Всёок!
                    el.setAttribute('disabled','disabled');
                    var elParent = el.parentNode.parentNode;
                    var eltrfl = elParent.querySelector('.views-field-archive');
                    console.log(eltrfl);
                    eltrfl.textContent = "true";
                } else {
                    console.log('Что-то пошло не так');
                }
            };

            request.onerror = function() {
                // Есливсёнеок..
            };

            request.send();
        }
    });
});