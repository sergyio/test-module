<?php
function bill_falling_views_data()
{
    $data['bill_falling_log']['table']['group'] = t('Данные пaдений биллинга');

    $data['bill_falling_log']['table']['base'] = array(
        'field' => 'id',
        'title' => t('Данные пaдений биллинга'),
        'help' => t('Данные о неудачных попатках пользователей соединиться с биллингом'),
        'weight' => -10,
    );

    $data['bill_falling_log']['id'] = array(
        'title' => t('ID'),
        'help' => t('ID попытки входа'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['bill_falling_log']['type'] = array(
        'title' => t('Тип события'),
        'help' => t('Тип попытки входа.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['bill_falling_log']['fall_time'] = array(
        'title' => t('Время падения'),
        'help' => t('UNIX Метка времени, когда была зафиксированна ненормативная работа скриптов соединения с биллингом.'),
        'field' => array(
            'handler' => 'views_handler_field_date',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_date',
        ),
    );

    $data['bill_falling_log']['load_time'] = array(
        'title' => t('Время загрузки'),
        'help' => t('Обозначает как долго осуществлялось соединение с биллингом.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['bill_falling_log']['status'] = array(
        'title' => t('Статус'),
        'help' => t('Статус окончания операции'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['bill_falling_log']['archive'] = array(
        'title' => t('Актуальность'),
        'help' => t('Обозначает обработанна ли заявка'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );
    // Вторая таблица
    $data['falling_user_data']['table']['group'] = t('Данные пaдений биллинга');

    $data['falling_user_data']['table']['base'] = array(
        'field' => 'id',
        'title' => t('Данные пользователя'),
        'help' => t('Данные пользователя при падении биллинга.'),
        'weight' => -10,
    );

    $data['falling_user_data']['table']['join'] = array(
        'bill_falling_log' => array(
            'left_field' => 'id',
            'field' => 'falling_id',
        ),
    );

    $data['falling_user_data']['falling_id'] = array(
        'title' => t('ID падения'),
        'help' => t('Идентификатор падения биллинга.'),
        'relationship' => array(
            'base' => 'bill_falling_log',
            'base field' => 'id',
            'handler' => 'views_handler_relationship',
            'label' => t('Default label for the relationship'),
            'title' => t('Title shown when adding the relationship'),
            'help' => t('More information on this relationship'),
        ),
    );

    $data['falling_user_data']['name'] = array(
        'title' => t('Имя'),
        'help' => t('Имя пользователя'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['falling_user_data']['email'] = array(
        'title' => t('Почта'),
        'help' => t('Почта пользователя'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['falling_user_data']['data'] = array(
        'title' => t('Данные'),
        'help' => t('Данные о заказе пользователя.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    return $data;
}