<?php

function bill_falling_views_default_views()
{

$view = new view();
$view->name = 'b_falling_stat';
$view->description = 'Полная статистика лога падений биллинга за всё время';
$view->tag = 'default';
$view->base_table = 'bill_falling_log';
$view->human_name = 'Статистика падений биллинга';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'нет';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'ещё';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
$handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
$handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
/* Поле: Данные пaдений биллинга: ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Время загрузки */
$handler->display->display_options['fields']['load_time']['id'] = 'load_time';
$handler->display->display_options['fields']['load_time']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['load_time']['field'] = 'load_time';
$handler->display->display_options['fields']['load_time']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Время падения */
$handler->display->display_options['fields']['fall_time']['id'] = 'fall_time';
$handler->display->display_options['fields']['fall_time']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['fall_time']['field'] = 'fall_time';
$handler->display->display_options['fields']['fall_time']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['fall_time']['date_format'] = 'medium';
$handler->display->display_options['fields']['fall_time']['custom_date_format'] = 'r';
$handler->display->display_options['fields']['fall_time']['second_date_format'] = 'html5_tools_iso8601';
$handler->display->display_options['fields']['fall_time']['timezone'] = 'Europe/Moscow';
/* Поле: Данные пaдений биллинга: Данные */
$handler->display->display_options['fields']['data']['id'] = 'data';
$handler->display->display_options['fields']['data']['table'] = 'falling_user_data';
$handler->display->display_options['fields']['data']['field'] = 'data';
$handler->display->display_options['fields']['data']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Имя */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'falling_user_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Почта */
$handler->display->display_options['fields']['email']['id'] = 'email';
$handler->display->display_options['fields']['email']['table'] = 'falling_user_data';
$handler->display->display_options['fields']['email']['field'] = 'email';
$handler->display->display_options['fields']['email']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Статус */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Тип события */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['type']['field'] = 'type';
/* Поле: Данные пaдений биллинга: Актуальность */
$handler->display->display_options['fields']['archive']['id'] = 'archive';
$handler->display->display_options['fields']['archive']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['archive']['field'] = 'archive';
$handler->display->display_options['fields']['archive']['label'] = 'Неактуально';
/* Критерий фильтра: Данные пaдений биллинга: Статус */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Статус';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['status']['group_info']['label'] = 'Статус';
$handler->display->display_options['filters']['status']['group_info']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Успешно',
    'operator' => '=',
    'value' => 'true',
  ),
  2 => array(
    'title' => 'Потрачено',
    'operator' => '=',
    'value' => 'false',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Тип события */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Тип события';
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['type']['group_info']['label'] = 'Тип события';
$handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
  3 => array(
    'title' => 'Регистрация',
    'operator' => 'contains',
    'value' => 'registration',
  ),
  4 => array(
    'title' => 'Вход',
    'operator' => 'contains',
    'value' => 'login',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Тип события */
$handler->display->display_options['filters']['type_1']['id'] = 'type_1';
$handler->display->display_options['filters']['type_1']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['type_1']['field'] = 'type';
$handler->display->display_options['filters']['type_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['type_1']['expose']['operator_id'] = 'type_1_op';
$handler->display->display_options['filters']['type_1']['expose']['label'] = 'Тип события';
$handler->display->display_options['filters']['type_1']['expose']['operator'] = 'type_1_op';
$handler->display->display_options['filters']['type_1']['expose']['identifier'] = 'type_1';
$handler->display->display_options['filters']['type_1']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['type_1']['group_info']['label'] = 'Источник события';
$handler->display->display_options['filters']['type_1']['group_info']['identifier'] = 'type_1';
$handler->display->display_options['filters']['type_1']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Корзина',
    'operator' => 'contains',
    'value' => 'from_cart',
  ),
  2 => array(
    'title' => 'Обычная страница',
    'operator' => 'contains',
    'value' => 'from_simplePage',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Актуальность */
$handler->display->display_options['filters']['archive']['id'] = 'archive';
$handler->display->display_options['filters']['archive']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['archive']['field'] = 'archive';
$handler->display->display_options['filters']['archive']['exposed'] = TRUE;
$handler->display->display_options['filters']['archive']['expose']['operator_id'] = 'archive_op';
$handler->display->display_options['filters']['archive']['expose']['label'] = 'Актуальность';
$handler->display->display_options['filters']['archive']['expose']['operator'] = 'archive_op';
$handler->display->display_options['filters']['archive']['expose']['identifier'] = 'archive';
$handler->display->display_options['filters']['archive']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['archive']['group_info']['label'] = 'Неактуально';
$handler->display->display_options['filters']['archive']['group_info']['identifier'] = 'archive';
$handler->display->display_options['filters']['archive']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Актуально',
    'operator' => '=',
    'value' => 'false',
  ),
  2 => array(
    'title' => 'Обработано ',
    'operator' => '=',
    'value' => 'true',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Время падения */
$handler->display->display_options['filters']['fall_time']['id'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['fall_time']['field'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['exposed'] = TRUE;
$handler->display->display_options['filters']['fall_time']['expose']['operator_id'] = 'fall_time_op';
$handler->display->display_options['filters']['fall_time']['expose']['label'] = 'Время падения';
$handler->display->display_options['filters']['fall_time']['expose']['operator'] = 'fall_time_op';
$handler->display->display_options['filters']['fall_time']['expose']['identifier'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['fall_time']['group_info']['label'] = 'Период падения';
$handler->display->display_options['filters']['fall_time']['group_info']['identifier'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'За прошедшие сутки',
    'operator' => '>',
    'value' => array(
      'type' => 'offset',
      'value' => '-1 day',
      'min' => '',
      'max' => '',
    ),
  ),
  2 => array(
    'title' => 'За прошедшую неделю',
    'operator' => '>',
    'value' => array(
      'type' => 'offset',
      'value' => '-7 day',
      'min' => '',
      'max' => '',
    ),
  ),
  3 => array(
    'title' => 'За пол месяца',
    'operator' => '>',
    'value' => array(
      'type' => 'offset',
      'value' => '-14 day',
      'min' => '',
      'max' => '',
    ),
  ),
  4 => array(
    'title' => 'За месяц',
    'operator' => '>',
    'value' => array(
      'type' => 'offset',
      'value' => '-30 day',
      'min' => '',
      'max' => '',
    ),
  ),
);

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block');
$handler->display->display_options['block_description'] = 'Падения биллинга(все)';
$translatables['b_falling_stat'] = array(
  t('Master'),
  t('нет'),
  t('ещё'),
  t('Применить'),
  t('Сбросить'),
  t('Сортировать по'),
  t('По возрастанию'),
  t('По убыванию'),
  t('ID'),
  t('Время загрузки'),
  t('.'),
  t(','),
  t('Время падения'),
  t('Данные'),
  t('Имя'),
  t('Почта'),
  t('Статус'),
  t('Тип события'),
  t('Неактуально'),
  t('Источник события'),
  t('Актуальность'),
  t('Block'),
  t('Падения биллинга(все)'),
);


/* 
    *** Второе представление ***
 */
$view_1 = new view();
$view_1->name = 'b_falling_actual';
$view_1->description = 'Актуальная статистика лога падений биллинга за последние сутки';
$view_1->tag = 'default';
$view_1->base_table = 'bill_falling_log';
$view_1->human_name = 'Статистика падений биллинга для отдела продаж';
$view_1->core = 7;
$view_1->api_version = '3.0';
$view_1->disabled = FALSE; /* Edit this to true to make a default view_1 disabled initially */

/* Display: Master */
$handler = $view_1->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'нет';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'ещё';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
$handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
$handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
/* Поле: Данные пaдений биллинга: ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Время загрузки */
$handler->display->display_options['fields']['load_time']['id'] = 'load_time';
$handler->display->display_options['fields']['load_time']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['load_time']['field'] = 'load_time';
$handler->display->display_options['fields']['load_time']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Время падения */
$handler->display->display_options['fields']['fall_time']['id'] = 'fall_time';
$handler->display->display_options['fields']['fall_time']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['fall_time']['field'] = 'fall_time';
$handler->display->display_options['fields']['fall_time']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['fall_time']['date_format'] = 'medium';
$handler->display->display_options['fields']['fall_time']['custom_date_format'] = 'r';
$handler->display->display_options['fields']['fall_time']['second_date_format'] = 'html5_tools_iso8601';
$handler->display->display_options['fields']['fall_time']['timezone'] = 'Europe/Moscow';
/* Поле: Данные пaдений биллинга: Данные */
$handler->display->display_options['fields']['data']['id'] = 'data';
$handler->display->display_options['fields']['data']['table'] = 'falling_user_data';
$handler->display->display_options['fields']['data']['field'] = 'data';
$handler->display->display_options['fields']['data']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Имя */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'falling_user_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Почта */
$handler->display->display_options['fields']['email']['id'] = 'email';
$handler->display->display_options['fields']['email']['table'] = 'falling_user_data';
$handler->display->display_options['fields']['email']['field'] = 'email';
$handler->display->display_options['fields']['email']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Статус */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
/* Поле: Данные пaдений биллинга: Тип события */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['type']['field'] = 'type';
/* Поле: Данные пaдений биллинга: Актуальность */
$handler->display->display_options['fields']['archive']['id'] = 'archive';
$handler->display->display_options['fields']['archive']['table'] = 'bill_falling_log';
$handler->display->display_options['fields']['archive']['field'] = 'archive';
$handler->display->display_options['fields']['archive']['label'] = 'Неактуально';
/* Критерий фильтра: Данные пaдений биллинга: Статус */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Статус';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['status']['group_info']['label'] = 'Статус';
$handler->display->display_options['filters']['status']['group_info']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Успешно',
    'operator' => '=',
    'value' => 'true',
  ),
  2 => array(
    'title' => 'Потрачено',
    'operator' => '=',
    'value' => 'false',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Тип события */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Тип события';
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['type']['group_info']['label'] = 'Тип события';
$handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
  3 => array(
    'title' => 'Регистрация',
    'operator' => 'contains',
    'value' => 'registration',
  ),
  4 => array(
    'title' => 'Вход',
    'operator' => 'contains',
    'value' => 'login',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Тип события */
$handler->display->display_options['filters']['type_1']['id'] = 'type_1';
$handler->display->display_options['filters']['type_1']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['type_1']['field'] = 'type';
$handler->display->display_options['filters']['type_1']['operator'] = 'contains';
$handler->display->display_options['filters']['type_1']['value'] = 'from_cart';
$handler->display->display_options['filters']['type_1']['expose']['operator_id'] = 'type_1_op';
$handler->display->display_options['filters']['type_1']['expose']['label'] = 'Тип события';
$handler->display->display_options['filters']['type_1']['expose']['operator'] = 'type_1_op';
$handler->display->display_options['filters']['type_1']['expose']['identifier'] = 'type_1';
$handler->display->display_options['filters']['type_1']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['type_1']['group_info']['label'] = 'Источник события';
$handler->display->display_options['filters']['type_1']['group_info']['identifier'] = 'type_1';
$handler->display->display_options['filters']['type_1']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Корзина',
    'operator' => 'contains',
    'value' => 'from_cart',
  ),
  2 => array(
    'title' => 'Обычная страница',
    'operator' => 'contains',
    'value' => 'from_simplePage',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Актуальность */
$handler->display->display_options['filters']['archive']['id'] = 'archive';
$handler->display->display_options['filters']['archive']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['archive']['field'] = 'archive';
$handler->display->display_options['filters']['archive']['value'] = 'false';
$handler->display->display_options['filters']['archive']['expose']['operator_id'] = 'archive_op';
$handler->display->display_options['filters']['archive']['expose']['label'] = 'Актуальность';
$handler->display->display_options['filters']['archive']['expose']['operator'] = 'archive_op';
$handler->display->display_options['filters']['archive']['expose']['identifier'] = 'archive';
$handler->display->display_options['filters']['archive']['is_grouped'] = TRUE;
$handler->display->display_options['filters']['archive']['group_info']['label'] = 'Актуальность';
$handler->display->display_options['filters']['archive']['group_info']['identifier'] = 'archive';
$handler->display->display_options['filters']['archive']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Актуально',
    'operator' => '=',
    'value' => 'false',
  ),
  2 => array(
    'title' => 'Обработано ',
    'operator' => '=',
    'value' => 'true',
  ),
);
/* Критерий фильтра: Данные пaдений биллинга: Время падения */
$handler->display->display_options['filters']['fall_time']['id'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['table'] = 'bill_falling_log';
$handler->display->display_options['filters']['fall_time']['field'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['operator'] = '>';
$handler->display->display_options['filters']['fall_time']['value']['value'] = '-1 day';
$handler->display->display_options['filters']['fall_time']['value']['type'] = 'offset';
$handler->display->display_options['filters']['fall_time']['expose']['operator_id'] = 'fall_time_op';
$handler->display->display_options['filters']['fall_time']['expose']['label'] = 'Время падения';
$handler->display->display_options['filters']['fall_time']['expose']['operator'] = 'fall_time_op';
$handler->display->display_options['filters']['fall_time']['expose']['identifier'] = 'fall_time';
$handler->display->display_options['filters']['fall_time']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);

/* Display: Block */
$handler = $view_1->new_display('block', 'Block', 'block');
$handler->display->display_options['block_description'] = 'Падения биллинга(актуальные)';
$translatables['b_falling_actual'] = array(
  t('Master'),
  t('нет'),
  t('ещё'),
  t('Применить'),
  t('Сбросить'),
  t('Сортировать по'),
  t('По возрастанию'),
  t('По убыванию'),
  t('ID'),
  t('Время загрузки'),
  t('.'),
  t(','),
  t('Время падения'),
  t('Данные'),
  t('Имя'),
  t('Почта'),
  t('Статус'),
  t('Тип события'),
  t('Неактуально'),
  t('Источник события'),
  t('Актуальность'),
  t('Block'),
  t('Падения биллинга(актуальные)'),
);

  // (Export ends here.)
  // Add view to list of views to provide.
    $views[$view->name] = $view;
    $views[$view_1->name] = $view_1;

    return $views;
}